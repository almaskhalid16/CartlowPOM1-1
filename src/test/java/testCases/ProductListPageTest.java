package testCases;

import base.BaseClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.LoginPage;
import pages.ProductListPage;

public class ProductListPageTest extends BaseClass {

    HomePage homepage;
    ProductListPage productListPage;
    public ProductListPageTest(){
        super();
    }
    @BeforeTest
    public void setUp() {
        initialization();
       // homepage = new HomePage();
        productListPage= new ProductListPage();
    }

    @Test(priority = 1)
    public void RandomClickHomeMenuTest() throws InterruptedException {
        productListPage.RandomClickHomeMenu();
    }

   @Test(priority = 2)
    public void addProductToWishListTest() throws InterruptedException {
        productListPage.addProductToWishList();
    }
   // @Test(priority = 3)
    public void filterPriceTest() throws InterruptedException {
        productListPage.filterPrice(20);
    }
    @Test(priority = 4)
    public void filterConditionsTest() throws InterruptedException {
        productListPage.filterConditions();
    }
   @Test(priority = 5)
    public void filterBrandsTest() throws InterruptedException {
        productListPage.filterBrands();
    }
   @Test(priority = 6)
    public void filterCategoriesTest() throws InterruptedException {
        productListPage.filterCategories();
    }
    @AfterTest
    public void tearDown(){
        driver.quit();
    }










    @AfterTest()
    public void teardown(){
        driver.quit();
    }

}
