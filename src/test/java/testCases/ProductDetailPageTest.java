package testCases;

import base.BaseClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.LoginPage;
import pages.ProductDetailPage;
import pages.ProductListPage;

public class ProductDetailPageTest extends BaseClass {

    ProductDetailPage productDetailPage;
    ProductListPage productListPage;

    public ProductDetailPageTest(){
        super();
    }

    @BeforeTest
    public void setUp() {
        initialization();
        productDetailPage = new ProductDetailPage();
    }
    @Test(priority = 1)
    public void navigateToProductsListPageTest() throws InterruptedException {
        productDetailPage.navigateToProductsListPage();
    }

   @Test(priority = 2)
    public void randomSelectProductTest() throws InterruptedException {
        productDetailPage.randomSelectProduct();
    }
   @Test(priority = 3)
    public void ProductDetailTest() throws InterruptedException {
        productDetailPage.ClickViewAllButton_return();
    }
    @Test(priority = 7)
    public void quantityCounterTest() throws InterruptedException {
        productDetailPage.quantityCounter();
    }
   @AfterTest
    public void tearDownBrowser(){
        driver.quit();
    }
}
