package testCases;

import base.BaseClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.LoginPage;
import pages.ProductListPage;

public class HomePageTest extends BaseClass {

   //LoginPage loginPage;
    HomePage homePage;
   // ProductListPage pListP;


    public HomePageTest(){
        super();
    }

    @BeforeTest
    public void setUp() {
        initialization();
        //loginPage = new LoginPage();
        //loginPage.ValidateLogin(prop.getProperty("username"), prop.getProperty("password"));
        //pListP = new   ProductListPage();
        homePage =new HomePage();
    }

    @Test(priority = 1)
    public void ChangeCountryTest() throws InterruptedException {
        homePage.changeCountry();
    }

    @Test(priority = 2)
    public void ChangeLanguageTest() throws InterruptedException {
        homePage.changeLanguage();
    }

    @Test(priority = 3)
    public void searchByStringTest() throws InterruptedException {
       homePage.SearchItem();
    }
    @AfterTest
    public void teardownBrowser(){
        driver.quit();
    }
}
