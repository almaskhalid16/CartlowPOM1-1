package testCases;

import base.BaseClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.LoginPage;
import pages.ProductListPage;
import utill.TestUtill;

import java.util.concurrent.TimeUnit;

public class LoginPageTest extends BaseClass {
    LoginPage loginPage;
   // HomePage homePage;
    ProductListPage pListP;

    public LoginPageTest(){
        super();
    }

    @BeforeTest
    public void setUp() {
        initialization();
        loginPage = new LoginPage();
        pListP = new   ProductListPage();
    }

    @Test(priority=1)
    public void ForgotPasswordTest() throws InterruptedException {
        loginPage.ForgotPassword("demo@cartlow.com");
    }

    @Test(priority=2)
    public void LoginFromHomeTest() throws InterruptedException {
     driver.manage().timeouts().implicitlyWait(TestUtill.page_Load_TimeOut, TimeUnit.SECONDS);
     loginPage.ValidateLogin(prop.getProperty("username"), prop.getProperty("password"));

    }

    @AfterTest
    public void TearDown(){
        driver.quit();
    }

}
