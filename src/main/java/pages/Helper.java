package pages;

import base.BaseClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.Random;

public class Helper  {

    BaseClass baseClass = new BaseClass();
    //Random Function

    public void randomFunction(int length, String xpath) {
        if (length > 0) {
            int randomProduct = (int) (Math.random() * length);
            List<WebElement> element = baseClass.driver.findElements(By.xpath(xpath));
            element.get(randomProduct).click();
        }
    }


}
