package pages;

import base.BaseClass;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.Random;

public class HomePage extends BaseClass {

    // Page Factory OR
    @FindBy (className = "flagBox")
    WebElement Country;
   /* @FindBy(xpath = "/html/body/div[1]/header/div/div[1]/div/div[2]/div/ul/li[1]/div/a/span[1]/img")
    WebElement Country;*/

    @FindBy(xpath = "//*[@id=\"getCountryForm\"]/li/a[1]")
    WebElement subCountry;

    @FindBy (xpath = "//div[@class=\"style2CenterBoxRightSide\"]/ul/li[2]")
    WebElement Language;

    @FindBy(xpath = "//*[@id=\"getLocalForm\"]/li[1]/a")
    WebElement subLanguage;

    @FindBy (xpath = "//*[@id=\"app-search\"]")
    WebElement searchBar;


    //Initializing the Page Objects:

    public HomePage() {
        PageFactory.initElements(driver, this);
    }

    //Actions

    //Change Country from Home Page
    public void changeCountry() throws InterruptedException {
        Thread.sleep(3000);
        Country.click();
        subCountry.click();
    }

    //Change Language from Home Page
    public void changeLanguage() throws InterruptedException {
        Thread.sleep(3000);
        Language.click();
        subLanguage.click();
    }
    public void SearchItem() throws InterruptedException {
        Thread.sleep(3500);
        String[] words = new String[]{"Lays","Toys","Flour","Milk"};
        Random rand = new Random();
        int SearchWord = rand.nextInt(words.length);
        searchBar.sendKeys(words[SearchWord] + Keys.ENTER);
        //return new ProductListPage();
    }





}
