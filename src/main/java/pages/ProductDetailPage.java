package pages;

import base.BaseClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class ProductDetailPage extends BaseClass {
    //Page Factory or Object Repository

    //productDetailPage
    @FindBy(xpath = "//section[@class=\"col-lg-9\"]/div[2]/div/div/div/div/div[3]/a")
    List<WebElement> productDetail;

    @FindBy(xpath = "//a[@class=\"btn btn-primary btn-sm\"]")
    WebElement viewAllButton;

    @FindBy(xpath = "//*[@id='var_quantity']")
    WebElement quantity;

    @FindBy(xpath = "//*[@id=\"addToCartForm\"]/div[1]/input[3]")
    WebElement increment;

    @FindBy(xpath = "//*[@id=\"addToCartForm\"]/div[1]/input[1]")
    WebElement decrement;

    @FindBy(xpath = "//*[@id=\"addToCartForm\"]/div[2]/div[1]/div/button")
    WebElement addToCart;


    //Initializing the Page Objects:
    public ProductDetailPage() {
        PageFactory.initElements(driver, this);
    }

    //Actions
    public void navigateToProductsListPage() throws InterruptedException {
        Thread.sleep(2500);
        ProductListPage productListPage = new ProductListPage();
        productListPage.RandomClickHomeMenu();
    }

    public void randomSelectProduct() throws InterruptedException {

        Thread.sleep(2500);
        String Products = "//section[@class='col-lg-9']/div[2]/div/div/div/div/div[3]/a";
        randomFunction(Products, "randomSelectProduct");

    }
    public void ClickViewAllButton_return() throws InterruptedException {
        Thread.sleep(3500);
        // WebElement viewAllButton = driver.findElement(By.xpath(ae.ViewAll));
        viewAllButton.click();
        Thread.sleep(1500);
        driver.navigate().back();
        Thread.sleep(3500);
    }
    public void quantityCounter() throws InterruptedException {
        Thread.sleep(3500);
        String getQuantity = quantity.getText();
        int convertQuantity = Integer.parseInt(getQuantity);

        if (convertQuantity > 1) {
            System.out.println("Available Quantity : "+convertQuantity);
            increment.click();
            Thread.sleep(1500);
            decrement.click();
            Thread.sleep(1500);
            addToCart.click();
            Thread.sleep(1500);
            driver.navigate().back();
        }else{
            System.out.println("Product Quantity is 1");

        }
    }

}


