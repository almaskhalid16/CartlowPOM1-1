package pages;

import base.BaseClass;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;
import java.util.Random;

public class ProductListPage extends BaseClass {

    public int size;
    // Page Factory OR

   /* @FindBy (xpath = "//*[@id=\"maxPrice\"]")
    WebElement maxPrice;

    @FindBy(xpath = "//div[2]/ul/li//label")
     List<WebElement> conditions;

    @FindBy (xpath ="//div[3]/ul/li//label" )
    List<WebElement> brands ;

    // View Cart Items //
    String cart = "//*[@id=\"cart-updater\"]";
    String viewCartItems = "//*[@id=\"cart-updater\"]/div/div/a[1]";
*/
    //<<<<<<<<<<<<>>>>>>>>>>>>>>>>>//

    @FindBy(xpath = "/html/body/div[1]/header/div/div[2]/div/div/ul[2]/li/a")
    List<WebElement> Products;

    //addProductToWishList
    @FindBy(xpath = "//section[@class=\"col-lg-9\"]/div[2]/div/div/div/div/div[2]/button")
    List<WebElement> wishlisitIcon;

    @FindBy (xpath = "//div[@id='signin-modal']/child::div/child::div/child::div/button/span")
    WebElement CloseBtn;

    //filterPrice
    @FindBy(xpath = "//*[@id=\"maxPrice\"]")
    WebElement maxPrice;

    //filterConditions
    @FindBy(xpath = "//div[2]/ul/li//label")
    List<WebElement> conditions;

    //filterBrands
    @FindBy(xpath = "//div[3]/ul/li//label")
    List<WebElement> brands;


    @FindBy(xpath = "//div[4]/ul/li//label")
    List<WebElement> categories;

    //Initializing the Page Objects:
    public ProductListPage() {
        PageFactory.initElements(driver, this);
    }

    //Randomly click on menu in header on Home Page
    public void RandomClickHomeMenu() throws InterruptedException {
        Thread.sleep(2500);
        String xpath="/html/body/div[1]/header/div/div[2]/div/div/ul[2]/li/a";
        randomFunction(xpath, "RandomClickHomeMenu");
    }

    //Click on Wishlish icon in Product card
    public void addProductToWishList() throws InterruptedException {
        Thread.sleep(4500);
        String xpath ="//section[@class='col-lg-9']/div[2]/div/div/div/div/div[2]/button";
        randomFunction(xpath,"addProductToWishList");
        Thread.sleep(4500);
        //Close login Popup
        CloseBtn.click();
    }

    //Filter Price
    public void filterPrice(int range) throws InterruptedException {
        Actions action = new Actions(driver);
        Thread.sleep(2500);
        // Price //
        maxPrice.click();
        maxPrice.sendKeys(Keys.chord(Keys.CONTROL, "a") + range + Keys.ENTER);
    }

    //Filter Conditions
    public void filterConditions() throws InterruptedException {
        Actions action = new Actions(driver);
        Thread.sleep(4500);
        // Conditions //
        String conditionPath ="//div[2]/ul/li//label";
        randomFunction(conditionPath,"filterConditions");
      /*  int getConditionsSize = conditions.size();
        Random randConditions = new Random();
        int indexConditions = randConditions.nextInt(getConditionsSize);
        action.moveToElement(conditions.get(indexConditions));
        action.click(conditions.get(indexConditions));
        action.build().perform();*/
    }

    //Filter Brands
    public void filterBrands() throws InterruptedException {
        Actions action = new Actions(driver);
        Thread.sleep(4500);
        // Brands //
        String brandsPath ="//div[3]/ul/li//label";
        randomFunction(brandsPath, "filterBrands" );
       /* int getBrandSize = brands.size();
        Random randBrands = new Random();
        int indexBrands = randBrands.nextInt(getBrandSize);
        action.moveToElement(brands.get(indexBrands));
        action.click(brands.get(indexBrands));
        action.build().perform();*/
    }

    // Filter Categories
    public void filterCategories() throws InterruptedException {
        Actions action = new Actions(driver);
        Thread.sleep(4500);
        // Categories //
        String CategoriesPath ="//div[4]/ul/li//label";
        randomFunction(CategoriesPath, "filterCategories");
/*
        int getCategoriesSize = categories.size();
        Random randCategories = new Random();
        int indexCategories = randCategories.nextInt(getCategoriesSize);
        System.out.println(indexCategories);
        action.moveToElement(categories.get(indexCategories));
        action.click(categories.get(indexCategories));
        action.build().perform();*/
    }


    }






