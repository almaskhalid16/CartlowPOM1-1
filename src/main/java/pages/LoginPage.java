package pages;

import base.BaseClass;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utill.TestUtill;

import java.util.concurrent.TimeUnit;

public class LoginPage extends BaseClass {

    //Page Factory
    @FindBy(xpath = "//span[text()='Sign in']")
    WebElement signInButton;

    @FindBy (xpath = "//div[@class=\"form-group d-flex flex-wrap justify-content-between\"]/a")
    WebElement forgotPasswordLink;

    @FindBy (xpath = "//*[@id=\"recover-email\"]")
    WebElement recoverMail;

    @FindBy (xpath = "//button[@class=\"btn btn-primary\"]")
    WebElement forgotButtonLink;

    @FindBy(xpath = "//input[@id='si-email']")
    WebElement email;

    @FindBy(xpath = "//input[@id='si-password']")
    WebElement password;

    @FindBy (xpath = "//button[@id='sign-in']")
    WebElement loginButton;


    //Actions
    public LoginPage() {
        PageFactory.initElements(driver, this);
    }
    public void ForgotPassword(String demoEmail) throws InterruptedException {
        Thread.sleep(3000);
        signInButton.click();
        Thread.sleep(1500);
        forgotPasswordLink.click();
        recoverMail.sendKeys(demoEmail);
        forgotButtonLink.click();
    }

    public void ValidateLogin(String un, String pwd) throws InterruptedException {

    Thread.sleep(3000);
    signInButton.click();
    Thread.sleep(1500);
    email.sendKeys(un);
    password.sendKeys(pwd);
    driver.manage().timeouts().implicitlyWait(TestUtill.IMPLISIT_WAIT, TimeUnit.SECONDS);
    loginButton.click();
    //return null;
   //return new HomePage();

}
}
