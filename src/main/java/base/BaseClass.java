package base;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import utill.TestUtill;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;


public class BaseClass {

    public static WebDriver driver;
    public static Properties prop;

    public BaseClass() {
        try {
            prop = new Properties();
            FileInputStream ip = new FileInputStream(System.getProperty("user.dir") + "/src/main/java/config/config.properties");
            prop.load(ip);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void initialization() {
        String browserName = prop.getProperty("browser");

        if (browserName.equals("chrome")) {
            System.setProperty("webdriver.chrome.driver", "C:\\selenium\\chromedriver.exe");
            driver = new ChromeDriver();
        } else if (browserName.equals("FF")) {
            System.setProperty("webdriver.gecko.driver", "C:\\selenium\\geckodriver.exe");
            driver = new FirefoxDriver();
        }
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().pageLoadTimeout(TestUtill.page_Load_TimeOut, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(TestUtill.IMPLISIT_WAIT, TimeUnit.SECONDS);

        driver.get(prop.getProperty("url"));
        String qr = prop.getProperty("qrCode");
        driver.findElement(By.xpath(qr)).click();

    }
    //Random Function
    public void randomFunction(String xpath, String randmMsg) {
        List<WebElement> element = driver.findElements(By.xpath(xpath));
        int length = element.size();
        int randomNumber = (int) (Math.random() * length);
        Actions actions = new Actions(driver);
        actions.moveToElement(element.get(randomNumber));
        actions.click(element.get(randomNumber));
        actions.build().perform();
        System.out.println(randmMsg+" : "+randomNumber);

    }
}

